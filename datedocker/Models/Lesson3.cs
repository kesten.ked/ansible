﻿namespace datedocker.Models
{
    public class Lesson3
    {
        public int Id { get; set; }
        public string Date { get; set; }

        public string Request { get; set; }

        public string Result { get; set; }

    }
}
