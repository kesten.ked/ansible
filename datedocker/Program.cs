using Microsoft.EntityFrameworkCore;
using SqlEfApi.Data;
using SqlEfApi.Logging;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Logging.ClearProviders()
    .AddDbLogger(configure => { });

// ��� Docker

builder.Services.AddDbContext<EFContext>(opt =>
{
    var config = builder.Configuration;
    // ��� �������� � ����������
    var server = config["DbServer"] ?? "51.250.5.79";
    // ��� �������� �� Windows
    // var server = config["DbServer"] ?? "localhost";
    var port = config["DbPort"] ?? "1433";
    var user = config["DbUser"] ?? "sa";
    var password = config["DbPassword"] ?? "PassCd12345678";

    var connectionString = $"Server={server},{port};Initial Catalog=SqlEfApi;User ID={user};Password={password}";

    opt.UseSqlServer(connectionString);
});


/*
builder.Services.AddDbContext<EFContext>(opt =>
{
    var config = builder.Configuration.GetSection("ConnectionStrings")
        .Get<ConnectionConfiguration>();
    opt.UseSqlServer(config.SqlEfApiConnection);
});
*/

// ������������ ���� sql
// builder.PopulateDB();

var app = builder.Build();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
