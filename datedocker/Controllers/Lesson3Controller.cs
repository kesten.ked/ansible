﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using datedocker.Models;
using datedocker.Services;
using SqlEfApi.Data;

namespace datedocker.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Lesson3Controller : ControllerBase
    {
        private readonly EFContext _context;
        private readonly CalendarService _calendarService = new();


        public Lesson3Controller(EFContext context)
        {
            _context = context;
        }

        // показать все поля
        [HttpGet("GetAllItems")]

        public IEnumerable<Lesson3> Get()
        {
            return _context.Lesson3;
        }

        // поиск записи по конкретному id
        [HttpGet("{id}")]
        public ActionResult<Lesson3> GetLesson3(int id)
        {
            var ls = _context.Lesson3.Find(id);

            if (ls == null)
            {
                return NotFound();
            }

            return Ok(ls);
        }

        // создание новой записи
        [HttpGet("Create")]
        public string GetDayOfWeek(string date)
        {
            Lesson3 lesson3 = new Lesson3();
            lesson3.Request = date;
            lesson3.Date = DateTime.Now.ToString("dd/MM/yy");



            lesson3.Result = _calendarService.GetDayOfWeek(date);



            _context.Lesson3.Add(lesson3);
            _context.SaveChanges();

            return _calendarService.GetDayOfWeek(date);

        }
        // ручная замена полей на произвольные значения
        [HttpPut("{id}")]
        public ActionResult<Lesson3> PutLesson3(int id, Lesson3 ls)
        {
            if (id != ls.Id)
                return BadRequest();

            _context.Entry(ls).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        // удаление по id
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            Lesson3 lesson3 = _context.Lesson3.Find(id);
            if (lesson3 is null)
            {
                return "This id was not found";
            }

            _context.Lesson3.Remove(lesson3);
            _context.SaveChanges();

            return "Success";
        }



    }
}
